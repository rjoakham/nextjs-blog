const globalConstants = {
    navigation: {
        logoText: "Russ' Next Blog",
        postsLink: 'Posts',
        contactLink: 'Contact',
    },
    homePageConstants: {
        metaTitle: "Russ' Next Blog",
        metaDescription: 'I post about programming and web development',
        heroTitle: "Hi I'm Russell",
        heroSubtitle:
            'I blog about web development - especially frontend frameworks such as React and Vue',
        featuredPostsTitle: 'Featured Posts',
    },
    allPostsConstants: {
        metaTitle: 'All Posts',
        metaDescription:
            'A list of all programming related tutorials and posts',
        pageTitle: 'All Posts',
    },
    contactPageConstants: {
        metaTitle: 'Contact Me!',
        metaDescription: 'Send me your messages!',
        pageTitle: 'How can i help you?',
        email: 'email',
        emailLabel: 'Your Email',
        name: 'name',
        nameLabel: 'Your Name',
        message: 'message',
        messageLabel: 'Your Message',
        buttonText: 'Send Message',
        pending: 'pending',
        pendingTitle: 'Sending message...',
        pendingMessage: 'Your message is on its way!',
        success: 'success',
        successTitle: 'Success!',
        successMessage: 'Message sent successfully!',
        error: 'error',
        errorTitle: 'Error!',
    },
}

export default globalConstants

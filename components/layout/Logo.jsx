import classes from './logo.module.css'

import globalConstants from '../../utilities/globalConstants'

function Logo() {
    const { logoText } = globalConstants.navigation
    return <div className={classes.logo}>{logoText}</div>
}

export default Logo

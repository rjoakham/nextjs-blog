import Link from 'next/link'

import Logo from './Logo'

import classes from './mainNavigation.module.css'

import globalConstants from '../../utilities/globalConstants'

function MainNavigation() {
    const { postsLink, contactLink } = globalConstants.navigation

    return (
        <header className={classes.header}>
            <Link href="/">
                <Logo />
            </Link>
            <nav>
                <ul>
                    <li>
                        <Link href="/posts">{postsLink}</Link>
                    </li>
                    <li>
                        <Link href="/contact">{contactLink}</Link>
                    </li>
                </ul>
            </nav>
        </header>
    )
}

export default MainNavigation

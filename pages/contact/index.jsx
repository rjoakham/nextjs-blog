import Head from 'next/head'

import ContactForm from '../../components/contact/ContactForm'

import globalConstants from '../../utilities/globalConstants'

function ContactPage() {
    const { metaTitle, metaDescription } = globalConstants.contactPageConstants

    return (
        <>
            <Head>
                <title>{metaTitle}</title>
                <meta name="description" content={metaDescription} />
            </Head>
            <ContactForm />
        </>
    )
}

export default ContactPage

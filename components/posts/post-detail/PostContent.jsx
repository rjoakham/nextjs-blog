import Image from 'next/image'
import ReactMarkdown from 'react-markdown'
import { PrismLight as SyntaxHighlighter } from 'react-syntax-highlighter'
import atomDark from 'react-syntax-highlighter/dist/cjs/styles/prism/atom-dark'
import js from 'react-syntax-highlighter/dist/cjs/languages/prism/javascript'
import css from 'react-syntax-highlighter/dist/cjs/languages/prism/css'

import PostHeader from './PostHeader'

import classes from './postContent.module.css'

SyntaxHighlighter.registerLanguage('js', js)
SyntaxHighlighter.registerLanguage('css', css)

function PostContent(props) {
    const { post } = props
    const { content, slug, title } = post
    const imagePath = `/images/posts/${slug}/${post.image}`

    const customRenderers = {
        // eslint-disable-next-line react/no-unstable-nested-components
        img(image) {
            return (
                <div className={classes.image}>
                    <Image
                        src={`${image.src}`}
                        alt={image.alt}
                        width={600}
                        height={300}
                    />
                </div>
            )
        },
        // eslint-disable-next-line react/no-unstable-nested-components
        p(paragraph) {
            const { node } = paragraph

            if (node.children[0].tagName === 'img') {
                const image = node.children[0]

                return (
                    <div className={classes.image}>
                        <Image
                            src={`${image.properties.src}`}
                            alt={image.properties.alt}
                            width={600}
                            height={300}
                        />
                    </div>
                )
            }

            return <p>{paragraph.children}</p>
        },
        // eslint-disable-next-line react/no-unstable-nested-components, no-shadow
        code({ node, inline, className, children, ...props }) {
            const match = /language-(\w+)/.exec(className || '')
            return !inline && match ? (
                <SyntaxHighlighter
                    // eslint-disable-next-line react/no-children-prop
                    children={String(children).replace(/\n$/, '')}
                    style={atomDark}
                    language={match[1]}
                    PreTag="div"
                    {...props}
                />
            ) : (
                <code className={className} {...props}>
                    {children}
                </code>
            )
        },
    }

    return (
        <article className={classes.content}>
            <PostHeader title={title} image={imagePath} />
            <ReactMarkdown components={customRenderers}>
                {content}
            </ReactMarkdown>
        </article>
    )
}

export default PostContent

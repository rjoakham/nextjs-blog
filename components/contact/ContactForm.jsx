import { useEffect, useState } from 'react'

import Notification from '../ui/Notification'
import sendContactData from '../../pages/api/sendContactData'

import classes from './contactForm.module.css'

import globalConstants from '../../utilities/globalConstants'

function ContactForm() {
    const {
        buttonText,
        email,
        emailLabel,
        message,
        messageLabel,
        name,
        nameLabel,
        pageTitle,
        pending,
        pendingMessage,
        pendingTitle,
        success,
        successMessage,
        successTitle,
        error,
        errorTitle,
    } = globalConstants.contactPageConstants

    const [enteredEmail, setEnteredEmail] = useState('')
    const [enteredName, setEnteredName] = useState('')
    const [enteredMessage, setEnteredMessage] = useState('')
    const [requestStatus, setRequestStatus] = useState()
    const [requestError, setRequestError] = useState('')

    // eslint-disable-next-line consistent-return
    useEffect(() => {
        if (requestStatus === success || requestStatus === error) {
            const timer = setTimeout(() => {
                setRequestError('')
                setRequestStatus(null)
            }, 3000)

            return () => clearTimeout(timer)
        }
    }, [requestStatus])

    function clearInputs() {
        setEnteredEmail('')
        setEnteredName('')
        setEnteredMessage('')
    }

    async function sendMessageHandler(event) {
        event.preventDefault()

        setRequestStatus(pending)

        try {
            await sendContactData({
                email: enteredEmail,
                name: enteredName,
                message: enteredMessage,
            })
            setRequestStatus(success)
            clearInputs()
        } catch (err) {
            setRequestError(err.message)
            setRequestStatus(error)
        }
    }

    let notification

    if (requestStatus === pending) {
        notification = {
            status: pending,
            title: pendingTitle,
            message: pendingMessage,
        }
    }

    if (requestStatus === success) {
        notification = {
            status: success,
            title: successTitle,
            message: successMessage,
        }
    }

    if (requestStatus === error) {
        notification = {
            status: error,
            title: errorTitle,
            message: requestError,
        }
    }

    return (
        <section className={classes.contact}>
            <h1>{pageTitle}</h1>
            <form onSubmit={sendMessageHandler} className={classes.form}>
                <div className={classes.controls}>
                    <div className={classes.control}>
                        <label htmlFor={email}>{emailLabel}</label>
                        <input
                            type={email}
                            id={email}
                            value={enteredEmail}
                            onChange={(event) =>
                                setEnteredEmail(event.target.value)
                            }
                            required
                        />
                    </div>
                    <div className={classes.control}>
                        <label htmlFor={name}>{nameLabel}</label>
                        <input
                            type="text"
                            id={name}
                            value={enteredName}
                            onChange={(event) =>
                                setEnteredName(event.target.value)
                            }
                            required
                        />
                    </div>
                </div>
                <div className={classes.control}>
                    <label htmlFor={message}>{messageLabel}</label>
                    <textarea
                        name={message}
                        id={message}
                        rows="5"
                        value={enteredMessage}
                        onChange={(event) =>
                            setEnteredMessage(event.target.value)
                        }
                    />
                </div>
                <div className={classes.actions}>
                    <button type="submit">{buttonText}</button>
                </div>
            </form>
            {notification && (
                <Notification
                    status={notification.status}
                    title={notification.title}
                    message={notification.message}
                />
            )}
        </section>
    )
}

export default ContactForm

import PostsGrid from './PostsGrid'

import classes from './allPosts.module.css'

import globalConstants from '../../utilities/globalConstants'

function AllPosts(props) {
    const { posts } = props
    const { pageTitle } = globalConstants.allPostsConstants

    return (
        <section className={classes.posts}>
            <h1>{pageTitle}</h1>
            <PostsGrid posts={posts} />
        </section>
    )
}

export default AllPosts

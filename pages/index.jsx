import Head from 'next/head'

import Hero from '../components/home-page/Hero'
import FeaturedPosts from '../components/home-page/FeaturedPosts'
import { getFeaturedPosts } from '../lib/posts-util'

import globalConstants from '../utilities/globalConstants'

function HomePage(props) {
    const { metaTitle, metaDescription } = globalConstants.homePageConstants
    const { posts } = props

    return (
        <>
            <Head>
                <title>{metaTitle}</title>
                <meta name="description" content={metaDescription} />
            </Head>
            <Hero />
            <FeaturedPosts posts={posts} />
        </>
    )
}

export function getStaticProps() {
    const featuredPosts = getFeaturedPosts()

    return {
        props: {
            posts: featuredPosts,
        },
    }
}

export default HomePage

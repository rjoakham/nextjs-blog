import Image from 'next/image'

import classes from './hero.module.css'
import globalConstants from '../../utilities/globalConstants'

function Hero() {
    const { heroTitle, heroSubtitle } = globalConstants.homePageConstants

    return (
        <section className={classes.hero}>
            <div className={classes.image}>
                <Image
                    src="/images/site/russ-red-chair.webp"
                    alt="An image showing Russell"
                    width={300}
                    height={300}
                />
            </div>
            <h1>{heroTitle}</h1>
            <p>{heroSubtitle}</p>
        </section>
    )
}

export default Hero

import { MongoClient } from 'mongodb'

const { env } = process

async function contactFormHandler(req, res) {
    if (req.method === 'POST') {
        const { email, message, name } = req.body

        if (
            !email ||
            !email.includes('@') ||
            !name ||
            name.trim() === '' ||
            !message ||
            message.trim() === ''
        ) {
            res.status(422).json({
                message: 'Invalid input.',
            })
            return
        }

        const newMessage = {
            email,
            name,
            message,
        }

        let client

        const connectionString = `mongodb+srv://${env.MONGODB_USER}:${env.MONGODB_PASS}@${env.MONGODB_CLUSTER}.z0cxe.mongodb.net/${env.MONGODB_DATABASE}?retryWrites=true&w=majority`

        try {
            client = await MongoClient.connect(connectionString)
        } catch (err) {
            res.status(500).json({ message: 'Could not connect to database' })
            return
        }

        const db = client.db()

        try {
            const result = await db.collection('messages').insertOne(newMessage)
            newMessage.id = result.insertedId
        } catch (err) {
            client.close()
            res.status(500).json({ message: 'Storing message failed' })
            return
        }

        client.close()

        res.status(201).json({
            message: `Successfully stored message! ${newMessage}`,
        })
    }
}

export default contactFormHandler

import Image from 'next/image'

function PostContentImage(props) {
    const { slug, image } = props

    return (
        <Image
            src={`/images/posts/${slug}/${image.src}`}
            alt={image.alt}
            width={600}
            height={300}
        />
    )
}

export default PostContentImage

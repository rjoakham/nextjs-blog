import classes from './featuredPosts.module.css'

import globalConstants from '../../utilities/globalConstants'
import PostsGrid from '../posts/PostsGrid'

function FeaturedPosts(props) {
    const { posts } = props
    const { featuredPostsTitle } = globalConstants.homePageConstants

    return (
        <section className={classes.latest}>
            <h2>{featuredPostsTitle}</h2>
            <PostsGrid posts={posts} />
        </section>
    )
}

export default FeaturedPosts

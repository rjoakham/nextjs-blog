import Head from 'next/head'

import PostContent from '../../components/posts/post-detail/PostContent'
import { getPostData, getPostsFiles } from '../../lib/posts-util'

function PostDetailPage(props) {
    const { post } = props
    const { title, excerpt } = post

    return (
        <>
            <Head>
                <title>{title}</title>
                <meta name="description" content={excerpt} />
            </Head>
            <PostContent post={post} />
        </>
    )
}

export function getStaticProps(ctx) {
    const { params } = ctx
    const { slug } = params

    const postData = getPostData(slug)

    return {
        props: {
            post: postData,
        },
        revalidate: 600,
    }
}

export function getStaticPaths() {
    const postFileNames = getPostsFiles()

    const slugs = postFileNames.map((fileName) => fileName.replace(/\.md$/, ''))

    return {
        paths: slugs.map((slug) => ({ params: { slug } })),
        fallback: false,
    }
}

export default PostDetailPage

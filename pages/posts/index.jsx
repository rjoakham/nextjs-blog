import Head from 'next/head'

import AllPosts from '../../components/posts/AllPosts'
import { getAllPosts } from '../../lib/posts-util'

import globalConstants from '../../utilities/globalConstants'

function AllPostsPage(props) {
    const { metaTitle, metaDescription } = globalConstants.allPostsConstants
    const { posts } = props

    return (
        <>
            <Head>
                <title>{metaTitle}</title>
                <meta name="description" content={metaDescription} />
            </Head>
            <AllPosts posts={posts} />
        </>
    )
}

export function getStaticProps() {
    const allPosts = getAllPosts()

    return {
        props: {
            posts: allPosts,
        },
    }
}

export default AllPostsPage

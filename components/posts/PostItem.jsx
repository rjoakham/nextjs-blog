import Link from 'next/link'
import Image from 'next/image'

import classes from './postItem.module.css'

function PostItem(props) {
    const { post } = props
    const { title, image, excerpt, date, slug } = post

    const formattedDate = new Date(date).toLocaleDateString('en-GB', {
        day: 'numeric',
        month: 'long',
        year: 'numeric',
    })

    const imagePath = `/images/posts/${slug}/${image}`
    const linkPage = `/posts/${slug}`

    return (
        <li className={classes.post}>
            <Link href={linkPage}>
                <div className={classes.image}>
                    <Image
                        alt={title}
                        src={imagePath}
                        priority
                        sizes="100%"
                        fill
                    />
                </div>
                <div className={classes.content}>
                    <h3>{title}</h3>
                    <time>{formattedDate}</time>
                    <p>{excerpt}</p>
                </div>
            </Link>
        </li>
    )
}

export default PostItem
